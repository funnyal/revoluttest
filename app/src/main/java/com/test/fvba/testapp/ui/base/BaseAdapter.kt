package com.test.fvba.testapp.ui.base

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class BaseAdapter<T>(protected var context: Context, var itemList: MutableList<T>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /* Public Abstract  Methods *********************************************************************/

    abstract fun createCustomViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder

    abstract fun onBindData(holder: RecyclerView.ViewHolder, `val`: T)

    abstract fun onItemViewType(pos: Int): Int

    /* Public Methods *******************************************************************************/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return createCustomViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBindData(holder, itemList[position])
    }

    override fun getItemViewType(position: Int): Int {
        return onItemViewType(position)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun addItems(savedCardItems: MutableList<T>) {
        itemList = savedCardItems
        this.notifyDataSetChanged()
    }

    fun addItem(singleItem: T) {
        itemList.add(singleItem)
        this.notifyDataSetChanged()
    }

    fun getItem(position: Int): T {
        return itemList[position]
    }

    fun removeItem(position: Int) {
        itemList.removeAt(position)
        notifyItemRemoved(position)
    }

    /* Private Methods ******************************************************************************/
    /* Getters / Setters ****************************************************************************/
    /* Inner classes ********************************************************************************/
}