package com.test.fvba.testapp.ui.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.test.fvba.testapp.R;
import com.test.fvba.testapp.util.ProgressDialog;


public class BaseNetworkActivity extends AppCompatActivity implements NetworkView {

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgress = new ProgressDialog(this);
    }

    public void showProgress() {
        mProgress.showProgressDialog();
    }
    public void hideProgress() {
        mProgress.hideProgressDialog();
    }

    public void showSnackBar(String message) {
        Snackbar.make(findViewById(R.id.root), message, Snackbar.LENGTH_LONG)
                .show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        hideProgress();
    }

    @Override
    public void onGenericError() {
        mProgress.hideProgressDialog();
        showSnackBar(getString(R.string.snackbar_generic_error));
    }

    @Override
    public void onNoConnection() {
        mProgress.hideProgressDialog();
        showSnackBar(getString(R.string.snackbar_no_connection));
    }

}
