package com.test.fvba.testapp.ui.main

interface IRateRowView {

    fun setCurrencyCode(code: String)
    fun setCurrencyValue(value: Double)
}