package com.test.fvba.testapp.ui.main

import android.content.Context
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.test.fvba.testapp.R
import com.test.fvba.testapp.data.model.Rate
import kotlinx.android.synthetic.main.item_currency.view.*


class RatesAdapter(private val presenter: MainPresenter<MainActivityView>, private var context: Context, private val itemClick: (Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TWO_DECIMAL_FORMAT = "%.2f"
    }

    /* Private Attributes ****************************************************************************/

    private var onBind: Boolean = true

    /* Public Methods *******************************************************************************/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun getItemCount(): Int {
        return presenter.getRatesRowsCount()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBind = true
        (holder as ViewHolder).bind(position, presenter.rates[position])
        onBind = false
    }

    fun updateDate(listRate: ArrayList<Rate>) {
        presenter.updateListRate(listRate)
        notifyDataSetChanged()
    }

    fun onItemClicked(position: Int) {
        presenter.updateRateListPositions(position)
        presenter.itemClicked = presenter.rates[position]
        notifyDataSetChanged()
    }

    /* Private Methods ******************************************************************************/

    private fun textWatcherListener(item: Rate, position: Int): TextWatcher? {
        return object : TextWatcher {
            override fun afterTextChanged(p0: Editable) {
            }

            override fun beforeTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence, start: Int, before: Int, count: Int) {
                presenter.setRowAfterTextChanged(p0.toString(), position, item)
                if (!onBind) {

                    Handler().post { notifyDataSetChanged() }
                }

            }

        }
    }


    /* Inner Classes ****************************************************************************/

    inner class ViewHolder(itemView: View, val listener: (Int) -> Unit) : RecyclerView.ViewHolder(itemView), IRateRowView {

        //        var flag: ImageView = itemView.iv_country_flag
        private var editTextCurrencyValue: EditText = itemView.et_rate

        fun bind(position: Int, item: Rate) {

            val textListener = textWatcherListener(item, position)
            if (position == 0) {
                editTextCurrencyValue.addTextChangedListener(textListener)
                editTextCurrencyValue.requestFocus(editTextCurrencyValue.toString().length)
            } else {
                editTextCurrencyValue.removeTextChangedListener(textListener)
            }

            itemView.container.setOnClickListener {
                listener(position)
            }

            presenter.onBindRateRowViewAtPosition(position, this)

//            val extendedCurrency = ExtendedCurrency.getCurrencyByISO(item.currencyCode)
//            itemView.iv_country_flag.setImageResource(extendedCurrency.flag)
        }

        override fun setCurrencyCode(code: String) {

            itemView.tv_currency_code.text = code
        }

        override fun setCurrencyValue(value: Double) {

            itemView.et_rate.setText(TWO_DECIMAL_FORMAT.format(value))
        }
    }


}