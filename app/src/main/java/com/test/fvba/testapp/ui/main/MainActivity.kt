package com.test.fvba.testapp.ui.main
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.test.fvba.testapp.R
import com.test.fvba.testapp.TestApplication
import com.test.fvba.testapp.data.model.Rate
import com.test.fvba.testapp.ui.base.BaseNetworkActivity
import com.test.fvba.testapp.util.Constants.Companion.DEFAULT_CURRENCY_CODE
import kotlinx.android.synthetic.main.content_main.*
import javax.inject.Inject

class MainActivity : BaseNetworkActivity(), MainActivityView{

    /* Public Attributes ****************************************************************************/

    @Inject
    lateinit var presenter: MainPresenter<MainActivityView>

    /* Private Attributes ****************************************************************************/

    private lateinit var  adapter: RatesAdapter

    /* Public Methods *******************************************************************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        TestApplication.getApplication(this).applicationComponent.inject(this)
        presenter.attachView(this)
        presenter.requestLatestRates(DEFAULT_CURRENCY_CODE, true)
        activateSwipeRefresh()

        adapter = RatesAdapter(presenter, this){
            onRateClicked(it)
        }
        rv_list.adapter = adapter

    }

    override fun showProgress() {
        super.showProgress()
        srl_swipe.isRefreshing = true
    }

    override fun hideProgress() {
        super.hideProgress()
        stopSwipeRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onLatestRatesRequestComplete(listRate: ArrayList<Rate>) {
        hideProgress()
        adapter.updateDate(listRate)
    }

    override fun onRateClicked(position: Int) {
        adapter.onItemClicked(position)
    }

    override fun onGenericError() {
        super.onGenericError()
        stopSwipeRefresh()
    }


    /* Private Methods ******************************************************************************/
    private fun stopSwipeRefresh() {
        if (srl_swipe != null) {
            srl_swipe.isRefreshing = false
        }
    }

    private fun activateSwipeRefresh() {
        srl_swipe.setOnRefreshListener{
            presenter.requestLatestRates(DEFAULT_CURRENCY_CODE, true)
        }
    }


}
