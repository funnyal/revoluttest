package com.test.fvba.testapp.util

class Constants {
    companion object {
        const val BASE_URL = "https://revolut.duckdns.org/"

        const val REQUEST_TIMEOUT = 300 //in seconds

        const val DEFAULT_CURRENCY_CODE = "EUR"

        const val ERROR_NO_CONNECTION = 999
        /**
         * Unexpected error
         */
        const val HTTP_GENERIC_ERROR_CODE = 998

        /**
         * Request Timeout error
         */
        const val HTTP_ERROR_CODE_TIMEOUT = 997
    }
}