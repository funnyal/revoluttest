package com.test.fvba.testapp.util

/**
 * Default subscriber base class to be used whenever you want default error handling.
 */
abstract class ResponseSubscriber<T> : rx.Subscriber<T>() {
    override fun onCompleted() {
        // no-op by default.
    }

    override fun onError(e: Throwable) {
        onResponseError(e)
    }

    override fun onNext(t: T) {
        // no-op by default.
    }

    private fun onResponseError(throwable: Throwable) {
        val resultCode = HTTPResponseHelper.parseError(throwable)
        when (resultCode) {
            Constants.HTTP_ERROR_CODE_TIMEOUT, Constants.ERROR_NO_CONNECTION -> onNoConnection()
            else -> onGenericError()
        }
    }

    abstract fun onNoConnection()

    abstract fun onGenericError()
}