package com.test.fvba.testapp.ui.main

import com.test.fvba.testapp.data.model.Rate
import com.test.fvba.testapp.ui.base.NetworkView

interface MainActivityView  : NetworkView {
    fun onLatestRatesRequestComplete(response: ArrayList<Rate>)
    fun onRateClicked(position: Int)

}