package com.test.fvba.testapp.data.repository

import com.test.fvba.testapp.data.DataManager
import com.test.fvba.testapp.data.model.LatestRatesResponse
import retrofit2.Response
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class RateRepository {

    companion object {
        @JvmStatic
        fun getLatestRates(dataManager: DataManager, currencyCode: String, subscriber: Subscriber<Response<LatestRatesResponse>>): Subscription =
                dataManager.appService.getRates(currencyCode)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber)
    }
    //could also have done this way nut I need to control subscriber
//        dataManager.appService.getRates(currencyCode)
//        .flatMapIterable { entries -> entries.body().rates.rates.entries }
//        .map { e -> returnList(e) }
//        .subscribeOn(Schedulers.io())
//        .observeOn(AndroidSchedulers.mainThread())


}