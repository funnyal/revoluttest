package com.test.fvba.testapp.data.model

data class LatestRatesResponse(
        val base: String,
        val date: String,
        val rates: Map<String, Double>
)