package com.test.fvba.testapp.ui.main

import com.test.fvba.testapp.data.model.Rate
import com.test.fvba.testapp.ui.base.MVPPresenter

interface IMainPresenter<V : MainActivityView> : MVPPresenter<V> {

    fun requestLatestRates(currency: String, showProgress:Boolean)
    fun onBindRateRowViewAtPosition(position: Int, itemView: IRateRowView)
    fun getRatesRowsCount(): Int
    fun setRowAfterTextChanged(textChanged: String, position: Int, item: Rate)
    fun updateRateListPositions(position: Int)
    fun updateListRate(listRate: ArrayList<Rate>)

}