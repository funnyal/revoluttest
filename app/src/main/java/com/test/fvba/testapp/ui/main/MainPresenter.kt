package com.test.fvba.testapp.ui.main

import com.test.fvba.testapp.data.DataManager
import com.test.fvba.testapp.data.model.LatestRatesResponse
import com.test.fvba.testapp.data.model.Rate
import com.test.fvba.testapp.data.repository.RateRepository
import com.test.fvba.testapp.ui.base.BasePresenter
import com.test.fvba.testapp.util.ResponseSubscriber
import com.test.fvba.testapp.util.RxUtil
import retrofit2.Response
import rx.Subscription
import javax.inject.Inject

class MainPresenter<V : MainActivityView> @Inject internal constructor() : BasePresenter<V>(), IMainPresenter<V> {

    /* Public Attributes ****************************************************************************/

    @Inject
    lateinit var dataManager: DataManager

    /* Private Attributes ****************************************************************************/

    private var subscription: Subscription? = null

    var rates: ArrayList<Rate> = arrayListOf()
    var originalRates: ArrayList<Rate> = arrayListOf()
    var itemClicked: Rate? = null

    /* Public Methods *******************************************************************************/
    override fun requestLatestRates(currency: String, showProgress: Boolean) {
        checkViewAttached()
        setProgressVisible(showProgress)

        subscription = RateRepository.getLatestRates(dataManager, currency, LatestRatesSubscriber())
    }

    override fun detachView() {
        super.detachView()
        RxUtil.unsubscribe(subscription)
    }

    private fun returnList(e: Map.Entry<String, Double>): ArrayList<Rate> {
        val listRates = arrayListOf<Rate>()
        listRates.add(Rate(e.key, e.value))
        rates = listRates
        originalRates = listRates
        return listRates
    }

    override fun onErrorHandler(throwable: Throwable) {
        setProgressVisible(false)
        getMvpView().onGenericError()
    }

    override fun onBindRateRowViewAtPosition(position: Int, itemView: IRateRowView) {
        itemView.setCurrencyCode(rates!![position].currencyCode)
        itemView.setCurrencyValue(rates!![position].currencyValue)
    }

    override fun updateListRate(listRate: ArrayList<Rate>) {
        rates = listRate
        originalRates = rates
    }

    override fun updateRateListPositions(position: Int) {
        val topList = rates!!.subList(0, position)
        val bottomList = rates!!.subList(position + 1, rates!!.size)
        val newList = listOf(rates!![position]) + topList + bottomList
        rates!!.clear()
        rates!!.addAll(newList)
    }

    override fun setRowAfterTextChanged(textChanged: String, position: Int, item: Rate) {
        if (!textChanged.isNullOrEmpty()) {
            val changedExchangeRate = textChanged.toDouble().div(originalRates!![position].currencyValue!!)
            item.currencyValue = textChanged.toDouble()
            rates!!.filter { it.currencyCode != item.currencyCode }.forEach {
                it.currencyValue = it.currencyValue * changedExchangeRate
            }

        }
    }

    override fun getRatesRowsCount(): Int {
        return rates.size
    }


    /* Private Methods ******************************************************************************/

    private fun onGetLatestRatesComplete(response: LatestRatesResponse) {
        val ratesList = response.rates.flatMap { e -> returnList(e) }
        getMvpView().onLatestRatesRequestComplete(ArrayList(ratesList))
    }

    /* Private Inner Classes ******************************************************************************/

    private inner class LatestRatesSubscriber : ResponseSubscriber<Response<LatestRatesResponse>>() {

        override fun onNext(response: Response<LatestRatesResponse>) {
            if (response.isSuccessful) {
                onGetLatestRatesComplete(response.body())

            } else {
                getMvpView().onGenericError()
            }
        }

        override fun onNoConnection() {
            getMvpView().onNoConnection()
        }

        override fun onGenericError() {
            getMvpView().onGenericError()
        }
    }


}