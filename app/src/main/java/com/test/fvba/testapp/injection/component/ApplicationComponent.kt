package com.test.fvba.testapp.injection.component

import com.test.fvba.testapp.injection.module.APIModule
import com.test.fvba.testapp.injection.module.ApplicationModule
import com.test.fvba.testapp.ui.main.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [APIModule::class, ApplicationModule::class])
interface ApplicationComponent {
    fun inject(activity: MainActivity)

}