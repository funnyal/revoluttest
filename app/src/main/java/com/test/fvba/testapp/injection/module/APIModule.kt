package com.test.fvba.testapp.injection.module

import android.content.Context
import com.google.gson.GsonBuilder
import com.test.fvba.testapp.data.api.ApiService
import com.test.fvba.testapp.data.api.CacheInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.OkHttpClient.Builder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by fvba on 6.4.18.
 */
@Module
open class APIModule {

    companion object {

        const val DEFAULT_TIMEOUT_SECONDS: Long = 60

    }

    private var mBaseUrl: String

    constructor(mBaseUrl: String) {
        this.mBaseUrl = mBaseUrl
    }


    @Provides
    @Singleton
    open fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(provideGson())
                .build()
    }

    @Provides
    @Singleton
    fun provideGson(): GsonConverterFactory {
        val gson = GsonBuilder()
                .setLenient()
                .serializeNulls()
                .create()
        return GsonConverterFactory.create(gson)
    }

    @Provides
    @Singleton
    fun provideCache(context: Context): Cache {
        val cacheSize = (5 * 1024 * 1024).toLong()
        return Cache(context.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache, cacheInterceptor: CacheInterceptor): OkHttpClient {
        val builder = Builder()
        builder.apply {
            connectTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            readTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            writeTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            cache(cache)
            addInterceptor(cacheInterceptor)
        }

        return builder.build()
    }


}