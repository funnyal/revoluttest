package com.test.fvba.testapp.data;

import com.test.fvba.testapp.data.api.ApiService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataManager {

    @Inject
    ApiService service;

    @Inject
    public DataManager() {}

    public ApiService getAppService() {
        return service;
    }
}
