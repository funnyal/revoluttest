package com.test.fvba.testapp.util;

import android.support.annotation.NonNull;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * HTTP response handling.
 */
public class HTTPResponseHelper {

    public static int parseError(Throwable throwable) {
        if (throwable instanceof SocketTimeoutException) {
            return Constants.HTTP_ERROR_CODE_TIMEOUT;
        } else if (throwable instanceof UnknownHostException) {
            return Constants.ERROR_NO_CONNECTION;
        }

        return Constants.HTTP_GENERIC_ERROR_CODE;
    }

    @NonNull
    public static <T> Response<T> getError(Throwable throwable) {
        return Response.error(Constants.HTTP_GENERIC_ERROR_CODE, ResponseBody
                .create(MediaType.parse("application/json"),
                        "{\"error\":[\"" + throwable.getLocalizedMessage() + "\"]}"));
    }
}

