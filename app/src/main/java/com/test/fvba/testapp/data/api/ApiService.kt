package com.test.fvba.testapp.data.api

import com.test.fvba.testapp.data.model.LatestRatesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

open interface ApiService{

    @GET("latest")
    open fun getRates(@Query("base") baseCurrencyCode: String): Observable<Response<LatestRatesResponse>>
}