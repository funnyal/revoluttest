package com.test.fvba.testapp.utils

import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.view.View
import android.widget.EditText
import org.hamcrest.Matcher

class ChildViewAction {

    companion object {

        fun clickChildViewWithId(
                id: Int): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View>? {
                    return null
                }

                override fun getDescription(): String {
                    return "Click on a child view val specified: withid.";
                }

                override fun perform(uiController: UiController,
                                     view: View) {
                    val v = view.findViewById<EditText>(id)
                    v.setText("8.08")
                }
            }
        }
    }
}