package com.test.fvba.testapp.activity

import android.app.Instrumentation
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.test.fvba.testapp.ApplicationTestComponent
import com.test.fvba.testapp.DaggerApplicationTestComponent
import com.test.fvba.testapp.R
import com.test.fvba.testapp.TestApplication
import com.test.fvba.testapp.data.api.ApiService
import com.test.fvba.testapp.data.model.LatestRatesResponse
import com.test.fvba.testapp.injection.module.ApplicationModule
import com.test.fvba.testapp.module.APITestModule
import com.test.fvba.testapp.runner.TestApplicationTestRunner
import com.test.fvba.testapp.ui.main.MainActivity
import com.test.fvba.testapp.util.Constants
import com.test.fvba.testapp.utils.ChildViewAction
import com.test.fvba.testapp.utils.CustomAssertions
import com.test.fvba.testapp.utils.mockResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.`when`
import retrofit2.Response
import rx.Observable
import javax.inject.Inject


@RunWith(AndroidJUnit4::class)
class MainActivityTest : TestApplicationTestRunner() {


    private var instrumentation: Instrumentation? = null

    private lateinit var testAppComponent: ApplicationTestComponent

    @Inject
    lateinit var apiService: ApiService

    @Rule
    @JvmField
    var mainActivity = ActivityTestRule(MainActivity::class.java)

    companion object {
        private const val TIME_OUT = 1000
        private const val MINIMUM_LIST_RATES_SIZE = 1
    }


    @Before
    fun setUp() {
        instrumentation = InstrumentationRegistry.getInstrumentation()
        val app = InstrumentationRegistry.getTargetContext().applicationContext as TestApplication
        testAppComponent = DaggerApplicationTestComponent.builder()
                .applicationModule(ApplicationModule(app))
                .aPIModule(APITestModule(Constants.BASE_URL))
                .build()
        app.applicationComponent = testAppComponent
        testAppComponent.inject(this)
    }

    @Test
    fun shouldBeAbleToLoadData() {

        val fakeListRate = mockResponse()

        // Set up the mock
        `when`<Observable<Response<LatestRatesResponse>>>(apiService.getRates(anyString())).thenReturn(Observable.just(Response.success(fakeListRate)))

        // Launch the activity
        mainActivity.launchActivity(Intent())

        onView(withId(R.id.rv_list)).check(CustomAssertions.hasMoreThan(MINIMUM_LIST_RATES_SIZE))

        mainActivity.activity.finish()
    }

    @Test
    fun shouldBeAbleToCalculateRate() {
        val fakeListRate = mockResponse()

        // Set up the mock
        `when`<Observable<Response<LatestRatesResponse>>>(apiService.getRates(anyString())).thenReturn(Observable.just(Response.success(fakeListRate)))

        // Launch the activity
        mainActivity.launchActivity(Intent())

        onView(withId(R.id.rv_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        onView(withId(R.id.rv_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ChildViewAction.clickChildViewWithId(R.id.et_rate)))


        mainActivity.activity.finish()
    }


}