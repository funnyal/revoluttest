package com.test.fvba.testapp.utils

import com.test.fvba.testapp.data.model.LatestRatesResponse

fun mockResponse(): LatestRatesResponse {
    val map = hashMapOf<String, Double>()
    map["AUD"] = 1.01
    map["BRL"] = 2.02
    map["USD"] = 4.04
    map["CZK"] = 6.06
    map["CAD"] = 8.08
    return LatestRatesResponse("EUR","10/10/2018", map)
}